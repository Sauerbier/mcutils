package de.sauerbier.mc.mcutils.config;

import org.apache.commons.lang.StringUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ConfigReader {

    public static void read(final JavaPlugin plugin){
        FileConfiguration configFile = plugin.getConfig();

        for (final Field field : plugin.getClass().getDeclaredFields()) {
            if(field.isAnnotationPresent(Config.class)){
                Config config = field.getDeclaredAnnotation(Config.class);
                String target = config.value();
                if(StringUtils.isEmpty(target)) target = field.getName();

                if(configFile.contains(config.value())){
                    field.setAccessible(true);
                    try {
                        field.set(plugin, map(target, field, configFile));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



    private static Object map(String path,Field field, FileConfiguration configFile){
        ConfigurationSection configurationSection = configFile.getConfigurationSection(path);

        if(configurationSection != null){
            try {
                Object current = field.getType().getDeclaredConstructor().newInstance();
                for (String key : configurationSection.getKeys(false)) {
                    String nextPath = path + "." + key;
                    Field declaredField = current.getClass().getDeclaredField(key);
                    if (Modifier.isTransient(declaredField.getModifiers())) continue;

                    declaredField.setAccessible(true);

                    if (declaredField.getType().isEnum()) {
                        declaredField.set(current, Enum.valueOf(((Class) declaredField.getType()), configFile.getString(nextPath)));
                    } else if (declaredField.getType().isPrimitive()) {
                        declaredField.set(current, configFile.get(nextPath));
                    } else {
                        Object map = map(nextPath, declaredField, configFile);
                        declaredField.set(current, map != null ? map : configFile.get(nextPath));
                    }

                }
                return current;
            }catch (NoSuchMethodException e){
                System.err.println("Error mapping config to object: Type " + field.getType().getName() + " does not have a default constructor!");
            } catch (Exception e) {
                System.err.println("Error mapping config to object at: " + path + " -> " + field.getName());
                System.err.println("Reason: " + e.getMessage());
            }
        }
        return null;
    }
}
