package de.sauerbier.mc.mcutils.concurrent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class PooledTimerFactory implements TimerFactory {

    private final ExecutorService pool;
    private final List<Timer> timers;
    private final AtomicInteger taskCounter;

    public PooledTimerFactory(String poolName) {
        //implement virtual Threads once minecraft supports jdk15
        this.pool = new ScheduledThreadPoolExecutor(1);
        this.timers = new ArrayList<>();
        this.taskCounter = new AtomicInteger();
    }

    @Override
    public Timer createTimer(TimerType type, Time time, Time tick, boolean repeat, ConcurrentCallback<Timer> callback) {
        return createTimer(type, time, tick, repeat, callback, null);
    }

    @Override
    public synchronized Timer createTimer(TimerType type, Time time, Time tick, boolean repeat, ConcurrentCallback<Timer> callback, ConcurrentCallback<Timer> onTick)  {
        Timer timer = null;

        try {
            Constructor<? extends Timer>  timerConstructor = type.getTimerClass().getDeclaredConstructor(ExecutorService.class, int.class, Time.class, Time.class, ConcurrentCallback.class, ConcurrentCallback.class);
            int id = this.taskCounter.incrementAndGet();
            timer = timerConstructor.newInstance(this.pool, id, time, tick, callback, onTick);
            timer.setRepeat(repeat);
            this.timers.add(timer);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            System.err.println("Could not instantiate Timer: " + type.getTimerClass().getName());
            e.printStackTrace();
        }
        return timer;
    }

    @Override
    public void close() {
        timers.forEach(Timer::stop);
        timers.clear();
        this.pool.shutdown();
    }
}
