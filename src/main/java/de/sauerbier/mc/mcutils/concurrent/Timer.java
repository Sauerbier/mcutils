package de.sauerbier.mc.mcutils.concurrent;

public interface Timer extends Runnable {

    void start();
    void stop();

    /**
     * Virtually restarts the Timer by resetting the time but keeps the task running
     */
    void restart();

    void setRepeat(boolean repeat);
    boolean isRepeat();

    long getTimeLeft();
}
