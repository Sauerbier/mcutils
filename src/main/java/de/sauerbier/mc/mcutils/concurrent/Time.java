package de.sauerbier.mc.mcutils.concurrent;

import java.util.Objects;

public final class Time{
    private TimeUnit unit;
    private long time;

    public Time(){

    }

    public Time(TimeUnit unit, long time){
        this.unit = unit;
        this.time = time;
    }

    public long toNanos(){
        return this.unit.toNanos(this.time) ;
    }


    public long toMicros(){
        return this.unit.toMicros(this.time);
    }

    public long toMillis(){
        return this.unit.toMillis(this.time);
    }

    public long toTicks(){
        return this.unit.toTicks(this.time);
    }

    public long toSeconds(){
        return this.unit.toSeconds(this.time);
    }

    public long toMinutes(){
        return this.unit.toMinutes(this.time);
    }

    public long toHours(){
        return this.unit.toHours(this.time);
    }

    public long toDays(){
        return this.unit.toDays(this.time);
    }

    public TimeUnit getUnit(){
        return unit;
    }

    public void setUnit(TimeUnit unit){
        this.unit = unit;
    }

    public long getTime(){
        return time;
    }

    public void setTime(long time){
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Time time1 = (Time) o;
        return time == time1.time && unit == time1.unit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(unit, time);
    }
}
