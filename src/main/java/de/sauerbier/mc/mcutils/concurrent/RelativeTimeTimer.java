package de.sauerbier.mc.mcutils.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public final class RelativeTimeTimer implements Timer {
    private static final long OVERHEAD = 5;

    private Time time, delay;
    private long timeLeft;
    private boolean alive = false, repeat = false, paused = false;
    private ConcurrentCallback<RelativeTimeTimer> callback, onTick;
    private ExecutorService executorService;
    private final int taskID;
    private Future<?> taskHandle;

    /**
     * Will trigger in a specified time interval (e.g. every 15min)
     * if timer is set to repeat, a new time has to be specified inside the callback to keep the timer running
     *
     * @param time     duration between each trigger of the timer
     * @param tick     how often the timer will refresh itself (more ticks -> higher accuracy of the trigger but also more resource heavy!)
     * @param callback executes when the timers triggers
     */
    protected RelativeTimeTimer(ExecutorService executorService, int taskID, Time time, Time tick, ConcurrentCallback<RelativeTimeTimer> callback) {
        this(executorService, taskID, time, tick, callback, null);
    }

    /**
     * Will trigger in a specified time interval (e.g. every 15min)
     * if timer is set to repeat, a new time has to be specified inside the callback to keep the timer running
     *
     * @param time     duration between each trigger of the timer
     * @param tick     how often the timer will refresh itself (more ticks -> higher accuracy of the trigger but also more resource heavy!)
     * @param callback executes when the timers triggers
     * @param onTick   executes on each tick of the timer
     */
    protected RelativeTimeTimer(ExecutorService executorService, int taskID, Time time, Time tick, ConcurrentCallback<RelativeTimeTimer> callback, ConcurrentCallback<RelativeTimeTimer> onTick) {
        this.executorService = executorService;
        this.taskID = taskID;
        this.delay = tick;
        this.callback = callback;
        this.onTick = onTick;
        setTime(time);
    }

    public synchronized void start() {
        if (!alive) {
            this.alive = true;
            this.taskHandle = this.executorService.submit(this);
        }
    }

    public void run() {
        while (alive) {
            if (timeLeft > 0) {
                if (onTick != null) {
                    onTick.call(this);
                }

                if (!paused)
                    timeLeft -= delay.toMillis();
            } else {
                try {
                    if (callback != null) {
                        callback.call(this);
                    }
                } catch (Exception e) {
                    System.err.println("Exception caught in relative time Timer: rtt-" + this.taskID);
                    e.printStackTrace();
                }

                if (!repeat) stop();
                else setTime(time);
                continue;
            }

            try {
                Thread.sleep(this.delay.toMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public synchronized void stop() {
        alive = false;
        if (callback != null) {
            callback.call(this);
        }
        this.timeLeft = time.getUnit().toMillis(time.getTime());
    }

    public synchronized void pause() {
        this.paused = true;
    }

    public synchronized void resume() {
        this.paused = false;
    }

    public synchronized void restart() {
        if (callback != null) {
            callback.call(this);
        }
        this.timeLeft = time.getUnit().toMillis(time.getTime());
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
        this.timeLeft = time.toMillis();
    }

    public Time getDelay() {
        return delay;
    }

    public void setDelay(Time delay) {
        this.delay = delay;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isPaused() {
        return paused;
    }
}
