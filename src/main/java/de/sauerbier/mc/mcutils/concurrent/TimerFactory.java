package de.sauerbier.mc.mcutils.concurrent;

import java.io.Closeable;

public interface TimerFactory extends Closeable {

    Timer createTimer(TimerType type, Time time, Time tick, boolean repeat, ConcurrentCallback<Timer> callback);
    Timer createTimer(TimerType type, Time time, Time tick, boolean repeat, ConcurrentCallback<Timer> callback, ConcurrentCallback<Timer> onTick);

    @Override
    void close();
}
