package de.sauerbier.mc.mcutils.concurrent;

@FunctionalInterface
public interface ConcurrentCallback<T> {
    void call( T type );
}
