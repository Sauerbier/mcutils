package de.sauerbier.mc.mcutils.concurrent;

public enum TimerType {

    ABSOLUTE(TimestampTimer.class),
    RELATIVE(RelativeTimeTimer.class);

    private Class<? extends Timer> timerClass;

    TimerType(Class<? extends Timer> clazz) {
        this.timerClass = clazz;
    }

    public Class<? extends Timer> getTimerClass() {
        return timerClass;
    }
}
