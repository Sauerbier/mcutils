package de.sauerbier.mc.mcutils.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public final class TimestampTimer implements Timer {
    private Time time;
    private Time delay;
    private boolean alive, repeat;
    private ConcurrentCallback<TimestampTimer> callback, onTick;
    private ExecutorService executorService;
    private final int taskID;
    private Future<?> taskHandle;

    /**
     * Will trigger on a fixed timestamp in the future (time can be changed on runtime with {@link TimestampTimer#setTime(Time)})
     * By default this timer will stop after the timestamp has been reached
     * if {@link TimestampTimer#setRepeat(boolean)} = true, a new timestamp can be defined inside the callback which will keep
     * the timer running towards the new timestamp. (e.g. trigger at 12pm -> update timestamp to next day at 12pm)
     *
     * @param time     timestamp on which the timer should trigger
     * @param tick     how often the timer will refresh itself (more ticks -> higher accuracy of the trigger but also more resource heavy!)
     * @param callback executes when the timers triggers
     */
    protected TimestampTimer(ExecutorService executorService, int taskID, Time time, Time tick, ConcurrentCallback<TimestampTimer> callback) {
        this(executorService, taskID, time, tick, callback, null);
    }


    /**
     * Will trigger on a fixed timestamp in the future (time can be changed on runtime with {@link TimestampTimer#setTime(Time)})
     * By default this timer will stop after the timestamp has been reached
     * if {@link TimestampTimer#setRepeat(boolean)} = true, a new timestamp can be defined inside the callback which will keep
     * the timer running towards the new timestamp. (e.g. trigger at 12pm -> update timestamp to next day at 12pm)
     *
     * @param time     timestamp on which the timer should trigger
     * @param tick     how often the timer will refresh itself (more ticks -> higher accuracy of the trigger but also more resource heavy!)
     * @param callback executes when the timers triggers
     * @param onTick   executes on each tick of the timer
     */
    protected TimestampTimer(ExecutorService executorService, int taskID, Time time, Time tick, ConcurrentCallback<TimestampTimer> callback, ConcurrentCallback<TimestampTimer> onTick) {
        this.executorService = executorService;
        this.taskID = taskID;
        this.time = time;
        this.delay = tick;
        this.callback = callback;
        this.onTick = onTick;
    }


    public synchronized void start() {
        if (!alive) {
            this.alive = true;
            this.taskHandle = this.executorService.submit(this);
        }
    }


    public void run() {
        while (alive) {
            if (System.currentTimeMillis() > time.toMillis()) {
                try {
                    callback.call(this);
                    if (!repeat) {
                        stop();
                        return;
                    }
                } catch (Exception e) {
                    System.err.println("Exception caught in Timer: tst-" + this.taskID);
                    e.printStackTrace();
                }
            }


            if (onTick != null) {
                onTick.call(this);
            }

            try {
                Thread.sleep(delay.toMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public synchronized void stop() {
        alive = false;
    }

    //restarting this timer does nothing since the time goal is absolut
    //so we will keep it running
    public synchronized void restart() {
    }

    public long getTimeLeft() {
        return time.toMillis() - System.currentTimeMillis();
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Time getDelay() {
        return delay;
    }

    public void setDelay(Time delay) {
        this.delay = delay;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }
}
